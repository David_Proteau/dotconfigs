" Use Vim settings, rather than Vi settings
" This must be first, because it changes other options as a side effect.
set nocompatible

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Sets how many lines of history VIM remembers
set history=100

" Set to auto read when a file is changed from the outside
set autoread

if has("gui_running")
  set term=win32
else
  set term=screen-256color
endif
set ttym=xterm2

augroup AutoReloadVimRC
  au!
  " automatically reload vimrc when it's saved
  au BufWritePost $MYVIMRC so $MYVIMRC
augroup END

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Other / looking for a better home settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Set the search scan to wrap around the file
set wrapscan

set cpoptions=$

set timeoutlen=500

" These commands open folds
set foldopen=block,insert,jump,mark,percent,quickfix,search,tag,undo

" For how *I* code there are the best types of settings for
" completion but I get rid of some neat things that you might like
set complete=.,w,b,t

set pastetoggle=<F5>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Graphical User interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if has("gui_running")
  set guioptions-=m  "remove menu bar
  set guioptions-=T  "remove toolbar
  set guioptions-=r  "remove right-hand scroll bar
  set guioptions-=L  "remove left-hand scroll bar

  "set fuoptions=maxvert,maxhorz
  "au GUIEnter * set fullscreen
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" User interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Allow backspacing over indent, eol, and the start of an insert
set backspace=2

set cursorline

" Tells Vim to always put a status line in, even if there is only one window
set laststatus=2

" Don't redraw while executing macros (good for performance)
set lazyredraw

set list

" For regular expressions turn magic on
set magic

" Line numbers
set relativenumber
set number

set ruler

" When the page start to scroll, keep the cursor 8 lines from
" the top and 8 lines from the bottom
set scrolloff=8

" Search settings
set incsearch       " Activate incremental search
set hlsearch        " highlight search terms

set showcmd

" Show matching parenthesis
set showmatch

set showmode


" set rtp+=~/.local/lib/python2.6/site-packages/powerline/bindings/vim
" Set the status line
" set stl=%f\ %m\ %r\ Line:\ %l/%L[%p%%]\ Col:\ %c\ Buf:\ #%n\ [%b][0x%B]

set statusline=\ %{HasPaste()}%<%-15.25(%f%)%m%r%h\ %w\ \ 
set statusline+=\ \ \ [%{&ff}/%Y] 
set statusline+=\ Line:\ %l/%L[%p%%]\ Col:\ %c\ [%b][0x%B]\ \ Buf:\ #%n\ 

au InsertEnter * call InsertStatuslineColor(v:insertmode)
au InsertLeave * hi statusline guibg=DarkGrey ctermfg=8 guifg=White ctermbg=15
au InsertLeave * hi StatusLine term=reverse ctermfg=7 ctermbg=2 gui=bold,reverse

" default the statusline to green when entering Vim
hi StatusLine term=reverse ctermfg=7 ctermbg=2 gui=bold,reverse guibg=DarkGreen guifg=White

set whichwrap+=<,>,h,l

" Set visual bell instead of beeping
set vb


if exists('$TMUX')
  let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
  let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
  let &t_SI = "\<Esc>]50;CursorShape=1\x7"
  let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif

" Make tabs, trailing whitespace, and non-breaking spaces visible
exec "set listchars=tab:\uBB\uBB,trail:\uB7,nbsp:~"


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Enable syntax highlighting
syntax enable

" set guifont=Monaco:h12.00
" set lines=35 columns=120

" Syntax coloring lines that are too long just slows down the world
set synmaxcol=2048

" Set color scheme
set t_Co=256
set background=dark
colorscheme xoria256
" set background=light
" colorscheme hemisu


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Files, backup and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Set utf8 as standard encoding
set encoding=utf-8

" Use Unix as the standard file type
set ffs=unix,dos,mac

" Turn backup off, thanks to Git
set nobackup
set noswapfile


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Text, tab and indent
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Use space instead of tabs
set expandtab

set smarttab

" Indent settings
set shiftwidth=2
set softtabstop=2

set autoindent
set copyindent      " copy previous indentation on autoindenting
set smartindent



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Mappings (c: command, n: normal, i: insert, v: visual)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Unmap the arrow keys
no <down> ddp
no <left> <Nop>
no <right> <Nop>
no <up> ddkP
" ino <down> <Nop>
" ino <left> <Nop>
" ino <right> <Nop>
" ino <up> <Nop>
vno <down> <Nop>
vno <left> <Nop>
vno <right> <Nop>
vno <up> <Nop>

" delete surrounding characters
noremap ds{ F{xf}x
noremap cs{ F{xf}xi
noremap ds" F"x,x
noremap cs" F"x,xi
noremap ds' F'x,x
noremap cs' F'x,xi
noremap ds( F(xf)x
noremap cs( F(xf)xi
noremap ds) F(xf)x
noremap cs) F(xf)xi

" CMAP / Alias
cnoremap W w

" Allow saving of files as sudo when I forgot to start vim using sudo
cmap w!! w !sudo tee > /dev/null %

" use abbreviation as a common typo corrector or fast insert
iabbr ture true
iabbr flase false

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! CurDir()
    let curdir = substitute(getcwd(), $HOME, "~", "")
    return curdir
endfunction

" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return '[PASTE]'
    else
        return ''
    endif
endfunction

function! InsertStatuslineColor(mode)
  if a:mode == 'i'
    hi statusline guibg=Cyan guifg=Black term=reverse ctermbg=6 ctermfg=15
  elseif a:mode == 'r'
    hi statusline guibg=Purple guifg=Black term=reverse ctermbg=1 ctermfg=15
  else
    hi statusline guibg=DarkRed guifg=Black term=reverse ctermbg=3 ctermfg=0
  endif
endfunction

