#!/bin/bash

CONFDIR=`pwd`

BASE_DIR_CONFIG='.config'
BASE_DIR_FISH=$BASE_DIR_CONFIG/fish
FFUNCTIONS=functions

TARGET_BASE=$CONFDIR/fish

LINK_FUNCTIONS=$BASE_DIR_FISH/$FFUNCTIONS
TARGET_FUNCTIONS=$TARGET_BASE/$FFUNCTIONS

cd ~
if [ ! -d $BASE_DIR_CONFIG ]; then
  echo "Creating config directory: $BASE_DIR_CONFIG"
  mkdir $BASE_DIR_CONFIG
else
  echo "Config directory exists: $BASE_DIR_CONFIG"
fi
if [ ! -d $BASE_DIR_FISH ]; then
  echo "Creating Fish Shell directory: $BASE_DIR_FISH"
  mkdir $BASE_DIR_FISH
else
  echo "Fish Shell directory exists: $BASE_DIR_FISH"
fi

if [ -L $LINK_FUNCTIONS ]; then
  rm $LINK_FUNCTIONS
fi
if [ -d $LINK_FUNCTIONS ]; then
  rm -rf $LINK_FUNCTIONS/
fi

echo "Creating link '$LINK_FUNCTIONS' from '$TARGET_FUNCTIONS'"
ln -s $TARGET_FUNCTIONS $LINK_FUNCTIONS

