# .bash_profile

# Get the aliases and functions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin
export PATH

# Screen workaround for WSL
export SCREENDIR=$HOME/.screen

### Load ~/.extra, ~/.bash_prompt, ~/.exports, ~/.aliases and ~/.functions
### ~/.extra can be used for settings you don.t want to commit

# Load ~/.bash/.bash_prompt, ~/.bash/.exports, ~/.bash/.aliases and ~/.bash/.bash_ssh-agent
for file in ~/.bash/.{bash_prompt,exports,aliases,bash_ssh-agent}; do
        [ -r "$file" ] && source "$file"
done
unset file

# init z   https://github.com/rupa/z
# . ~/code/z/z.sh

# init rvm
# source ~/.rvm/scripts/rvm

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob

# Prefer US English and use UTF-8
export LC_ALL="en_US.UTF-8"
export LANG="en_US"

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2)" scp sftp ssh

# Add tab completion for `defaults read|write NSGlobalDomain`
# You could just use `-g` instead, but I like being explicit
complete -W "NSGlobalDomain" defaults

