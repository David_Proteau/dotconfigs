#!/bin/bash

CONFDIR=`pwd`
BKUP_DIR='.bkup_dotfiles'

LINK_RC='.bashrc'
TARGET_RC=$CONFDIR/bash/$LINK_RC

LINK_PRFL='.bash_profile'
TARGET_PRFL=$CONFDIR/bash/$LINK_PRFL

LINK_DIR='.bash'
TARGET_DIR=$CONFDIR/bash/$LINK_DIR

#echo $LINK_RC
#echo $TARGET_RC

#echo $LINK_PRFL
#echo $TARGET_PRFL

#echo $LINK_DIR
#echo $TARGET_DIR

cd ~
if [ ! -d $BKUP_DIR ]; then
  echo "Creating backup directory: $BKUP_DIR"
  mkdir $BKUP_DIR
else
  echo "Backup directory exists: $BKUP_DIR"
fi
echo ""

for LINK in {$LINK_RC,$LINK_PRFL,$LINK_DIR}; do
  if [ -L "$LINK" ]; then
    echo "Removing link: $LINK"
    #rm $LINK
  fi
  if [ -d "$LINK" ]; then
    echo "Moving directory to backup directory: $LINK"
    mv $LINK $BKUP_DIR/$LINK
  elif [ -e "$LINK" ]; then
    echo "Moving file to backup directory: $LINK"
    mv $LINK $BKUP_DIR/$LINK
  fi
done
unset LINK
echo ""

#rm $LINK_RC
#rm $LINK_PRFL
#rm $LINK_DIR

echo "Creating link '$LINK_RC' from '$TARGET_RC'"
ln -s $TARGET_RC $LINK_RC
echo "Creating link '$LINK_PRFL' from '$TARGET_PRFL'"
ln -s $TARGET_PRFL $LINK_PRFL
echo "Creating link '$LINK_DIR' from '$TARGET_DIR'"
ln -s $TARGET_DIR $LINK_DIR

