#!/bin/bash

LINK='.screenrc'
TARGET=`pwd`/gnuscreen/.screenrc

echo $LINK
echo $TARGET

cd ~
rm $HOME/$LINK

ln -s $TARGET $LINK

# Workaround for WSL
if [ -d $HOME/.screen ]; then
   echo "Removing SCREENDIR target"
   rm -rf $HOME/.screen
fi
echo "Creating SCREENDIR target"
mkdir ~/.screen && chmod 700 ~/.screen
export SCREENDIR=$HOME/.screen

