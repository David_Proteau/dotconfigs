#!/bin/bash

CONFDIR=`pwd`

LINK_RC='.vimrc'
TARGET_RC=$CONFDIR/vim/.vimrc

LINK_DIR='.vim'
TARGET_DIR=$CONFDIR/vim/.vim

echo $LINK_RC
echo $TARGET_RC

echo $LINK_DIR
echo $TARGET_DIR

cd ~

rm $LINK_RC
rm $LINK_DIR

ln -s $TARGET_RC $LINK_RC
ln -s $TARGET_DIR $LINK_DIR

